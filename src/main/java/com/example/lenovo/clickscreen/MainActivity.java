package com.example.lenovo.clickscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import java.util.Deque;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static int clicks = 0;
    @BindView(R.id.button)
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    public void onClick() {
        if (clicks < 3) {
            clicks++;
        }
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                startOtherActivity();
            }
        };
        handler.postDelayed(runnable,  500);

    }

    public void startOtherActivity() {
        Intent intent = null;
        switch (clicks) {
            case 1:
                intent = new Intent(this, Activity2.class);
                break;
            case 2:
                intent = new Intent(this, Activity3.class);
                break;
            case 3:
                intent = new Intent(this, Activity4.class);
                break;
        }
        if(intent != null) {
            startActivity(intent);
        }
        clicks = 0;
    }
}
